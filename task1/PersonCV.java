/*
 * 
 */
import java.util.Arrays;

public final class PersonCV
{
	/* Here we have used char [] as a String */
	private final String personalDetails; //A dynamic string to store personal details whose size is only known at runtime
	private final String [] qualificationsList; //an array of Strings to store qualifications list
	private final String [] workingHistory; //A string to store working History.

	public static void main(String [] args)
	{
		String pd = "personal details in here";
		String [] ql = {"qualification1", "qualification2"};
		String [] wh = {"history1", "history2"};
		PersonCV p = new PersonCV(pd, ql, wh);

		System.out.println(pd);
		String [] arr = p.getQualificationsList();
		arr[0] = "newQual";
		System.out.println("Qaulification:"+p.getQualificationsList()[0]);

	}

	public String getPersonalDetails()
	{
		return this.personalDetails; //A String is immutable, so we can return the String itself
	}

	/* Return deep copy of qualifications array */
	public String [] getQualificationsList()
	{
		return Arrays.copyOf(this.qualificationsList, this.qualificationsList.length);
	}

	/* Return deep copy of working history array */
	public String [] getWorkingHistory()
	{
		return Arrays.copyOf(this.workingHistory, this.workingHistory.length);
	}

	/* Use the constructor to initialise an Immutable PersonCV object, afterwhich it becomes unchangeable from this initialisation */
	public PersonCV(String pd, String [] ql, String [] wh)
	{
		this.personalDetails = pd;
		this.qualificationsList = ql;
		this.workingHistory = wh;
	}
}
