import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import javax.swing.SwingWorker;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.awt.Dimension;
//import java.util.concurrent.ExecutionException;

public class LoadCSV implements Runnable
{
	MainFrame2 frame;
	Container container;
	JLabel timeLabel; 
	JButton button, csvButton; 
	static final int HEADERINDEX = 0;
	static final String FILENAME = "1500000 Sales Records.csv";
	static final String LOADING = "Loading CSV...";
	static final String LOADED  = "CSV loaded.";
	static final String LOAD = "Load CSV";
	static String [] headerArr = null;
	final JScrollPane scrollpane;
	//JTextArea textArea; //must be final. Move to container or main if causing problems
	//JScrollPane scrollpane; //must be final
	Date date;
	DateFormat dateFormat;

	SwingWorker<String[][], Void> loadCSV = new SwingWorker<String[][], Void>()
		{
			public String[][] doInBackground()
			{
				String [][] data = getCSVdata(FILENAME);
				return data;
			}

			public void done()
			{
				System.out.println(headerArr[0]);
				String [][] data = null;
				try
				{
					data = get();
				}
				catch (InterruptedException ignore)
				{
					System.out.println("Loading of csv inturrupted");
					System.exit(1);
				}
				catch (java.util.concurrent.ExecutionException e)
				{
					System.out.println("Loading of csv reports exe exception");
					System.exit(1);
				}

				csvButton.setText(LOADED);
				JTable table = new JTable(data, headerArr);
				table.setPreferredScrollableViewportSize(new Dimension(500,50));
				table.setFillsViewportHeight(true);
				scrollpane.setViewportView(table);

			}
		};

	private static class TimerSwingWorker extends SwingWorker<Void, String>
	{
		final String STR_DATE_FORMAT = "HH:mm:ss";
		Container container;
		Date date;
		DateFormat dateFormat;
		String formattedDate;
		final JTextArea textArea;

		public TimerSwingWorker(Container cont)
		{
			container = cont;
			textArea = new JTextArea();
			date = new Date();
			dateFormat = new SimpleDateFormat(STR_DATE_FORMAT);
			String formattedDate = dateFormat.format(date);
			//timeLabel = new JLabel();
			//timeLabel.setVisible(true);

			container.add(textArea, BorderLayout.NORTH);
			textArea.setText(formattedDate);
			textArea.setEditable(false);
		}

		protected Void doInBackground()
		{
			String strDateFormat = "HH:mm:ss";
			while(true)
			{
				date = new Date();
				dateFormat = new SimpleDateFormat(strDateFormat);
				publish( dateFormat.format(date) );
			}

			//return null;
		}

		protected void process(List<String> times)
		{
			formattedDate = times.get( times.size() - 1);
			textArea.setText(formattedDate);
		}

		public void done()
		{
		}

	}

	public String [][] getCSVdata(String csvFilename)
	{
		boolean isHeader = true;
		ArrayList<String []> dataArrayList = new ArrayList<String[]>();
		try
		{
			Scanner sc = new Scanner(new File(csvFilename));
			while(sc.hasNextLine())
			{
				Scanner lineScanner = new Scanner(sc.nextLine()).useDelimiter(",");
				ArrayList<String> lineArrayList = new ArrayList<String>();
				while(lineScanner.hasNext())
				{
					lineArrayList.add( lineScanner.next());
				}
				String [] lineArr = new String[lineArrayList.size()];
				lineArrayList.toArray(lineArr);
				if( isHeader )
				{
					headerArr = new String[lineArrayList.size()];
					lineArrayList.toArray(headerArr);
					isHeader = false;
				}
				else
				{
					dataArrayList.add( lineArr );
				}
			}
		}
		catch( FileNotFoundException except)
		{
			System.out.println("Could not find specified file with filename '"+csvFilename+"'.");
		}
		String [][] data = new String[dataArrayList.size()][dataArrayList.get(HEADERINDEX).length];
		dataArrayList.toArray(data);
		return data;
	}

	public static void main(String [] args)
	{
		LoadCSV csvloader = new LoadCSV();
		try
		{
			SwingUtilities.invokeAndWait(csvloader);
		}
		catch (Exception e)
		{
			System.out.println("Caught Exception:"+e);
		}
		csvloader.drawGUI();
	}

	public void drawGUI()
	{
		button = new JButton("Days till christmas");
		csvButton = new JButton(LOAD);
		TimerSwingWorker timer = new TimerSwingWorker(container);
		timer.execute();

		container.add(button, BorderLayout.WEST);
		container.add(csvButton, BorderLayout.EAST);

		container.add(scrollpane, BorderLayout.CENTER);

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				JFrame messageFrame = new JFrame("Days till Christmas");
				Date date = new Date();
				String strDateFormat; 
				DateFormat dateFormat; 
				
				String christmas = "25/12/2019";
				Date christmasDate = null;
				try
				{
					strDateFormat = "dd/M/y";
					dateFormat = new SimpleDateFormat(strDateFormat);
					christmasDate = dateFormat.parse(christmas); //throws ParseException
				}
				catch( ParseException except)
				{
					JOptionPane.showMessageDialog(messageFrame, "Error parsing christmas day.");
				}
				long diffMillis = christmasDate.getTime() - date.getTime();
				long diffHours = diffMillis / (60 * 60 * 1000);
				long diffDays = diffHours/24;
				JOptionPane.showMessageDialog(messageFrame, "Christmas in: "+diffDays+" days");

			}
		});

		csvButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				JFrame messageFrame = new JFrame("CSV Message");
				if( csvButton.getText().equals(LOADING))
				{
					JOptionPane.showMessageDialog(messageFrame, "CSV Loading."); 
				}
				else
				if( csvButton.getText().equals(LOADED))
				{
					JOptionPane.showMessageDialog(messageFrame, "CSV already Loaded.");
				}
				else
				{
					csvButton.setText(LOADING);
					loadCSV.execute();
				}

			}
		});


	}

	//public class MyGUIdrawer extends JFrame
	public class MainFrame2 extends JFrame
	{
		public MainFrame2(String frameTitle)
		{
			super(frameTitle);
			setLayout(new BorderLayout());
		}
	}

	public LoadCSV()
	{
		frame = new MainFrame2("FrameName");
		container = frame.getContentPane();
		TimerSwingWorker timer;
		scrollpane = new JScrollPane();
	}

	public void run()
	{
		frame.setSize(1300, 900);
		JLabel label = new JLabel("Time");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}
}
