
import ds.util.DiGraph;

public class DistanceTable
{
	DiGraph<String> gtable;

	public int getDistance(String from, String to)
	{
		if( from.equals(to) )
			throw new IllegalArgumentException("The from location '"+from+"' may not be same as the to location '"+to+"'.");
		if( (from == null) || (to == null) )
			throw new IllegalArgumentException("Getting a distance requires both the to and from locations to be non null.");

		int weight = gtable.getWeight(from, to);

		if( weight == -1 )
			weight = gtable.getWeight(to, from);
		if( weight == -1 )
			throw new IllegalArgumentException("Distances between places may only be retrieved if they were inserted first.");

		return weight;
	}

	public void insertDistance(String from, String to, Integer distance )
	{
		if( from.equals(to) )
			throw new IllegalArgumentException("The from location '"+from+"' may not be same as the to location '"+to+"'.");
		if( (from == null) || (to == null) )
			throw new IllegalArgumentException("Getting a distance requires both the to and from locations to be non null.");

		gtable.addVertex(from);
		gtable.addVertex(to);
	}

	public DistanceTable()
	{
		gtable = new DiGraph<String>();
	}

	public static void main (String [] args)
	{
		DistanceTable test = new DistanceTable();
		test.insertDistance("Bloem","JHB", 5);
	}
}

